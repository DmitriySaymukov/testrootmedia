(function () {
    $(document).ready(function () {
        //прозрачность заголовков
        var opcol = $('.opacible');
        opcol.hover(
            function () {
                //hover on
                opcol.stop(true, true);
                opcol.not(this).animate({opacity: 0.5}, 800, 'linear');
            },
            function () {
                //hover off
                opcol.stop();
                opcol.animate({opacity: 1}, 200, 'linear');
            }
        );
        // подключение вкладок
        $(function () {
            $("#tabs").tabs();
        });
        // подключение слайдера
        var bxsl = $('.bxslider').bxSlider({
            auto: true,
            adaptiveHeight: true,
            pause: 10000,
            controls: false,
            mode: 'fade',
            pager: false,
            speed: 800
        });
        $('.bxslider-prev-control').click(function () {
            bxsl.goToPrevSlide();
        });
        $('.bxslider-next-control').click(function () {
            bxsl.goToNextSlide();
        });
        // выпадающий блок с брендами-магазинов
        var ci = $('.close-icon');
        var cb = $('.collapsed-block');
        var sb = $('.scrollable-block');
        var h = $('.tab-inner').css('height');
        var shopctrl = $('.show-shops-control');
        shopctrl.bind('click', shopctrl_click);

        function shopctrl_click () {
            shopctrl.unbind('click', shopctrl_click);
            ci.bind('hover', ci_hover);
            cb.css('visibility', 'visible');

            cb.animate({height: h}, 300, 'linear', function () {
                sb.css('opacity', 0);
                sb.css('visibility', 'visible');
                sb.animate({opacity: 1}, 300, 'linear');
                ci.css('opacity', 0);
                ci.css('visibility', 'visible');
                ci.animate({opacity: 1}, 300, 'linear');
            });

        }

        ci.click(function () {
            ci.unbind('mouseenter mouseleave', ci_hover());

            ci.stop();
            ci.animate({opacity: 0}, 300, 'linear', function () {
                ci.css('visibility', 'hidden');
            });
            sb.animate({opacity: 0}, 300, 'linear', function () {
                cb.animate({height: 0},300, 'linear', function () {
                    cb.css('visibility', 'hidden');
                });
            });
            shopctrl.bind('click', shopctrl_click);
        });

        function ci_hover() {
            ci.mouseenter(function () {
                ci.stop(true, false);
                ci.animate({opacity: 0.5}, 800, 'linear');
            });
            ci.mouseleave(function () {
                ci.stop();
                ci.animate({opacity: 1}, 800, 'linear');
            });
        }

    });
})();
